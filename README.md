# Geekstuff.dev / Devcontainers / Features / Docker

This devcontainer feature installs Docker client, fix some group ID weirdness in WSL and
mounts docker socket.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/docker": {}
    }
}
```

You can use any debian, ubuntu or alpine image as the base.

This feature requires the basics feature since it needs the non-root user.

The above will pull use latest version of that feature, otherwise with **an example**
`v1.2.3` tag in this project source code, you would be able to use tags such as:

- `example.registry/some/path/feature:1`
- `example.registry/some/path/feature:1.2`
- `example.registry/some/path/feature:1.2.3`
- `example.registry/some/path/feature:latest`

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/docker/-/tags).

## TODO

- Simplify imported .common script

## Source of the scripts

The scripts in this feature and others started in this group, originates from
a [different project](https://gitlab.com/geekstuff.it/devcontainers/)
that has similar ambitions but started before "Features" came along.

Many many iterations and tests were conducted both in CI and humanly so that those
legacy style features work reliably in most shells you would encounter and
definitely in all Debian, Ubuntu or Alpine based images.

This is their new and improved home! :)

Enjoy!
